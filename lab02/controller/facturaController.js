const facturaModel = require("../models/facturaModel");
const createFactura = async (req, res) => {
  const newFactura = new facturaModel(req.body);
  await newFactura.save();
  res.status(201).json(newFactura);
};
const getFacturas = async (req, res) => {
  const facturas = await facturaModel.find();
  res.status(200).json(facturas);
};
const getFacturaById = async (req, res) => {
  const factura = await facturaModel.findOne({ _id: req.params.id });
  res.status(200).json(factura);
};
const deleteFactura = async (req, res) => {
  await facturaModel.findByIdAndDelete(req.params.id);
  res.status(202).send("deleted:" + req.params.id);
};
const updateFactura = async (req, res) => {
  const facturaUpdated = await facturaModel.findByIdAndUpdate(
    req.params.id,
    req.body,
    { new: true }
  );
  res.status(202).send(facturaUpdated);
};
module.exports = {
  createFactura,
  getFacturas,
  getFacturaById,
  deleteFactura,
  updateFactura,
};
