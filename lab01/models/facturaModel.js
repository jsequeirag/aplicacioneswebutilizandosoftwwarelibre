const { Schema, model } = require("mongoose");
const facturaSchema = new Schema(
  {
    idArticulo: {
      type: Number,
      required: true,
    },
    precioUnitario: {
      type: Number,
      required: true,
    },
    cantidad: { type: Number, required: true },
  },
  { timestamps: true }
);

const facturaModel = model("factura", facturaSchema);
module.exports = facturaModel;
