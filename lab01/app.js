/* -------------------------------------------------------------------------- */
/*                                   packages                                 */
/* -------------------------------------------------------------------------- */
const dotEnv = require("dotenv");
const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const mongoose = require("mongoose");
/* -------------------------------------------------------------------------- */
/*                                   dotenv                                   */
/* -------------------------------------------------------------------------- */
dotEnv.config();
/* -------------------------------------------------------------------------- */
/*                               express server                               */
/* -------------------------------------------------------------------------- */
const app = express();

/* -------------------------------------------------------------------------- */
/*                                 middleware                                 */
/* -------------------------------------------------------------------------- */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan("dev"));
app.use(cors());
/* -------------------------------------------------------------------------- */
/*                                   routes                                   */
/* -------------------------------------------------------------------------- */
const facturaRoutes = require("./routes/facturaRoutes");
/* ------------------------------ facturaroutes ----------------------------- */
app.use("/factura/", facturaRoutes);
app.use("/", () => {
  console.log("index");
});
/* ---------------------- database conection-server run --------------------- */
mongoose
  .connect(process.env.MONGO)
  .then(() => {
    app.listen(process.env.PORT, () => {
      console.log("server:3000");
    });
  })
  .catch((e) => {
    console.log(e);
  });
